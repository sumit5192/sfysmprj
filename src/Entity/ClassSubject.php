<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClassSubjectRepository")
 */
class ClassSubject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SubjectList")
     */
    private $subject;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClassSection", inversedBy="classSubjects")
     */
    private $ClassSection;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Markssheet", mappedBy="subject")
     */
    private $markssheets;

    public function __construct()
    {
        $this->markssheets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?SubjectList
    {
        return $this->subject;
    }

    public function setSubject(SubjectList $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getClassSection(): ?ClassSection
    {
        return $this->ClassSection;
    }

    public function setClassSection(?ClassSection $ClassSection): self
    {
        $this->ClassSection = $ClassSection;

        return $this;
    }

    /**
     * @return Collection|Markssheet[]
     */
    public function getMarkssheets(): Collection
    {
        return $this->markssheets;
    }

    public function addMarkssheet(Markssheet $markssheet): self
    {
        if (!$this->markssheets->contains($markssheet)) {
            $this->markssheets[] = $markssheet;
            $markssheet->addSubject($this);
        }

        return $this;
    }

    public function removeMarkssheet(Markssheet $markssheet): self
    {
        if ($this->markssheets->contains($markssheet)) {
            $this->markssheets->removeElement($markssheet);
            $markssheet->removeSubject($this);
        }

        return $this;
    }
}
