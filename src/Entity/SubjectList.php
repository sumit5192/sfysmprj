<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubjectListRepository")
 */
class SubjectList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClassSubject", mappedBy="subject")
     */
    private $classSections;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ClassSection", mappedBy="subject")
     */
    private $markssheets;



    public function __construct()
    {
        $this->classSections = new ArrayCollection();
        $this->classSubjects = new ArrayCollection();
        $this->csb = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ClassSection[]
     */
    public function getClassSections(): Collection
    {
        return $this->classSections;
    }

    public function addClassSection(ClassSection $classSection): self
    {
        if (!$this->classSections->contains($classSection)) {
            $this->classSections[] = $classSection;
            $classSection->addSubject($this);
        }

        return $this;
    }

    public function removeClassSection(ClassSection $classSection): self
    {
        if ($this->classSections->contains($classSection)) {
            $this->classSections->removeElement($classSection);
            $classSection->removeSubject($this);
        }

        return $this;
    }

    /**
     * @return Collection|Markssheet[]
     */
    public function getMarkssheets(): Collection
    {
        return $this->markssheets;
    }

    public function addMarkssheet(Markssheet $markssheet): self
    {
        if (!$this->markssheets->contains($markssheet)) {
            $this->markssheets[] = $markssheet;
            $markssheet->addTest($this);
        }

        return $this;
    }

    public function removeMarkssheet(Markssheet $markssheet): self
    {
        if ($this->markssheets->contains($markssheet)) {
            $this->markssheets->removeElement($markssheet);
            $markssheet->removeTest($this);
        }

        return $this;
    }


}
