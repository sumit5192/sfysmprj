<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestMarkSheetRepository")
 */
class TestMarkSheet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $Date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Student", inversedBy="testMarkSheets")
     */
    private $Student;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClassSection", inversedBy="testMarkSheets")
     */
    private $ClassSection;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Markssheet", mappedBy="test",cascade={"remove"})
     */
    private $markssheets;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClassSubDivisionList", inversedBy="testMarkSheets")
     */
    private $subdivision;

    public function __construct()
    {
        $this->markssheets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->Student;
    }

    public function setStudent(?Student $Student): self
    {
        $this->Student = $Student;

        return $this;
    }

    public function getClassSection(): ?ClassSection
    {
        return $this->ClassSection;
    }

    public function setClassSection(?ClassSection $ClassSection): self
    {
        $this->ClassSection = $ClassSection;

        return $this;
    }

    /**
     * @return Collection|Markssheet[]
     */
    public function getMarkssheets(): Collection
    {
        return $this->markssheets;
    }

    public function addMarkssheet(Markssheet $markssheet): self
    {
        if (!$this->markssheets->contains($markssheet)) {
            $this->markssheets[] = $markssheet;
            $markssheet->addTest($this);
        }

        return $this;
    }

    public function removeMarkssheet(Markssheet $markssheet): self
    {
        if ($this->markssheets->contains($markssheet)) {

            $this->markssheets->removeElement($markssheet);
            $markssheet->removeTest($this);
        }

        return $this;
    }

    public function getSubdivision(): ?ClassSubDivisionList
    {
        return $this->subdivision;
    }

    public function setSubdivision(?ClassSubDivisionList $subdivision): self
    {
        $this->subdivision = $subdivision;

        return $this;
    }
}
