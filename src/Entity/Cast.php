<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CastRepository")
 */
class Cast
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\student", mappedBy="cast")
     */
    private $student;

    public function __construct()
    {
        $this->student = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|student[]
     */
    public function getStudent(): Collection
    {
        return $this->student;
    }

    public function addStudent(student $student): self
    {
        if (!$this->student->contains($student)) {
            $this->student[] = $student;
            $student->setCast($this);
        }

        return $this;
    }

    public function removeStudent(student $student): self
    {
        if ($this->student->contains($student)) {
            $this->student->removeElement($student);
            // set the owning side to null (unless already changed)
            if ($student->getCast() === $this) {
                $student->setCast(null);
            }
        }

        return $this;
    }


}
