<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarkssheetRepository")
 */
class Markssheet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SubjectList", inversedBy="markssheets")
     */
    private $subject;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $marks;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TestMarkSheet", inversedBy="markssheets")
     */
    private $test;

    public function __construct()
    {
        $this->subject = new ArrayCollection();
        $this->test = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|ClassSubject[]
     */
    public function getSubject(): Collection
    {
        return $this->subject;
    }

    public function addSubject(?SubjectList $subject): self
    {
        if (!$this->subject->contains($subject)) {
            $this->subject[] = $subject;
        }

        return $this;
    }

    public function removeSubject(?SubjectList $subject): self
    {
        if ($this->subject->contains($subject)) {
            $this->subject->removeElement($subject);
        }

        return $this;
    }

    public function getMarks(): ?float
    {
        return $this->marks;
    }

    public function setMarks(?float $marks): self
    {
        $this->marks = $marks;

        return $this;
    }

    /**
     * @return Collection|TestMarkSheet[]
     */
    public function getTest(): Collection
    {
        return $this->test;
    }

    public function addTest(TestMarkSheet $test): self
    {
        if (!$this->test->contains($test)) {
            $this->test[] = $test;
        }

        return $this;
    }

    public function removeTest(TestMarkSheet $test): self
    {
        if ($this->test->contains($test)) {
            $this->test->removeElement($test);
        }

        return $this;
    }
}
