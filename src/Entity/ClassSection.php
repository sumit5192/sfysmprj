<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClassSectionRepository")
 */
class ClassSection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClassSectionSubDivision", mappedBy="ClassSection",cascade={"persist","remove"})
     */
    private $classSectionSubDivisions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Student", mappedBy="ClassSection")
     */
    private $students;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClassSubject", mappedBy="ClassSection",cascade={"persist","remove"})
     */
    private $classSubjects;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TestMarkSheet", mappedBy="ClassSection")
     */
    private $testMarkSheets;



    public function __construct()
    {
        $this->classSectionSubDivisions = new ArrayCollection();
        $this->students = new ArrayCollection();
        $this->classSubjects = new ArrayCollection();
        $this->testMarkSheets = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ClassSectionSubDivision[]
     */
    public function getClassSectionSubDivisions(): Collection
    {
        return $this->classSectionSubDivisions;
    }

    public function addClassSectionSubDivision(ClassSectionSubDivision $classSectionSubDivision): self
    {
        if (!$this->classSectionSubDivisions->contains($classSectionSubDivision)) {
            $this->classSectionSubDivisions[] = $classSectionSubDivision;
            $classSectionSubDivision->setClassSection($this);
        }

        return $this;
    }

    public function removeClassSectionSubDivision(ClassSectionSubDivision $classSectionSubDivision): self
    {

        if ($this->classSectionSubDivisions->contains($classSectionSubDivision)) {
            $this->classSectionSubDivisions->removeElement($classSectionSubDivision);
            $classSectionSubDivision->removeClassSection($this);
        }

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setClassSection($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {

        if ($this->student->contains($student)) {
            $this->student->removeElement($student);
            $student->removeClassSection(null);
        }
        return $this;
    }

    /**
     * @return Collection|ClassSubject[]
     */
    public function getClassSubjects(): Collection
    {
        return $this->classSubjects;
    }

    public function addClassSubject(ClassSubject $classSubject): self
    {
        if (!$this->classSubjects->contains($classSubject)) {
            $this->classSubjects[] = $classSubject;
            $classSubject->setClassSection($this);
        }

        return $this;
    }

    public function removeClassSubject(ClassSubject $classSubject): self
    {
        if ($this->classSubjects->contains($classSubject)) {
            $this->classSubjects->removeElement($classSubject);
            $classSubject->removeClassSection($this);
        }

        return $this;
    }

    /**
     * @return Collection|TestMarkSheet[]
     */
    public function getTestMarkSheets(): Collection
    {
        return $this->testMarkSheets;
    }

    public function addTestMarkSheet(TestMarkSheet $testMarkSheet): self
    {
        if (!$this->testMarkSheets->contains($testMarkSheet)) {
            $this->testMarkSheets[] = $testMarkSheet;
            $testMarkSheet->setClassSection($this);
        }

        return $this;
    }

    public function removeTestMarkSheet(TestMarkSheet $testMarkSheet): self
    {
        if ($this->testMarkSheets->contains($testMarkSheet)) {
            $this->testMarkSheets->removeElement($testMarkSheet);
            // set the owning side to null (unless already changed)
            if ($testMarkSheet->getClassSection() === $this) {
                $testMarkSheet->setClassSection(null);
            }
        }

        return $this;
    }


}
