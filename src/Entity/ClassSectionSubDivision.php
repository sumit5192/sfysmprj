<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClassSectionSubDivisionRepository")
 */
class ClassSectionSubDivision
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClassSubDivisionList")
     */
    private $division;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClassSection", inversedBy="classSectionSubDivisions")
     */
    private $ClassSection;

    public function __construct()
    {
        //$this->ClassSection = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDivision(): ?ClassSubDivisionList
    {
        return $this->division;
    }

    public function setDivision(ClassSubDivisionList $division): self
    {
        $this->division = $division;

        return $this;
    }

    /**
     * @return Collection|ClassSection[]
     */
    public function getClassSection(): ?ClassSection
    {
        return $this->ClassSection;
    }

    public function setClassSection(?ClassSection $ClassSection): self
    {
        $this->ClassSection = $ClassSection;

        return $this;
    }
}
