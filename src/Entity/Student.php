<?php

namespace App\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AcmeAssert;



/**
 * @ORM\Entity(repositoryClass="App\Repository\StudentRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class Student
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100,unique=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */

    private $email;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private $mobile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cast", inversedBy="student")
     */
    private $cast;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Studentremarks", mappedBy="student",cascade={"persist"})
     *
     */
    private $remarks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Hobbie", mappedBy="student", cascade={"persist"}))
     */
    private $hobbies;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotNull
     */
    private $bithdate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $favourite_color;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClassSection", inversedBy="students")
     */
    private $ClassSection;

    /**
     *  @ORM\ManyToOne(targetEntity="App\Entity\ClassSubDivisionList", inversedBy="students")
     */
    private $ClassSubDivision;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TestMarkSheet", mappedBy="Student")
     */
    private $testMarkSheets;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StudentStatus", inversedBy="students")
     */
    private $status;


    public function __construct()
    {
        $this->remarks = new ArrayCollection();
        $this->hobbies = new ArrayCollection();
        $this->testMarkSheets = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMobile(): ?int
    {
        return $this->mobile;
    }

    public function setMobile(int $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getCast(): ?Cast
    {
        return $this->cast;
    }

    public function setCast(?Cast $cast): self
    {
        $this->cast = $cast;

        return $this;
    }

    /**
     * @return Collection|Studentremarks[]
     */
    public function getRemarks(): Collection
    {
        return $this->remarks;
    }

    public function addRemark(studentremarks $remark): self
    {
        if (!$this->remarks->contains($remark)) {
            $this->remarks[] = $remark;
            $remark->setStudent($this);
        }

        return $this;
    }

    public function removeRemark(studentremarks $remark): self
    {
        if ($this->remarks->contains($remark)) {
            $this->remarks->removeElement($remark);
            // set the owning side to null (unless already changed)
            if ($remark->getStudent() === $this) {
                $remark->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Hobbie[]
     */
    public function getHobbies(): Collection
    {
        return $this->hobbies;
    }

    public function addHobby(Hobbie $hobby): self
    {
        if (!$this->hobbies->contains($hobby)) {
            $this->hobbies[] = $hobby;
            $hobby->setStudent($this);
        }

        return $this;
    }

    public function removeHobby(Hobbie $hobby): self
    {
        if ($this->hobbies->contains($hobby)) {
            $this->hobbies->removeElement($hobby);
            // set the owning side to null (unless already changed)
            if ($hobby->getStudent() === $this) {
                $hobby->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function getBithdate(): ?\DateTimeInterface
    {
        return $this->bithdate;
    }

    public function setBithdate(\DateTimeInterface $bithdate): self
    {
        $this->bithdate = $bithdate;

        return $this;
    }

    public function getFavouriteColor()
    {
        return $this->favourite_color;

    }

    public function setFavouriteColor($favourite_color): self
    {
        $this->favourite_color = $favourite_color;

        return $this;
    }

    public function getClassSection(): ?ClassSection
    {
        return $this->ClassSection;
    }

    public function setClassSection(?ClassSection $ClassSection): self
    {
        $this->ClassSection = $ClassSection;

        return $this;
    }

    public function getClassSubDivision(): ?ClassSubDivisionList
    {
        return $this->ClassSubDivision;
    }

    public function setClassSubDivision(?ClassSubDivisionList $ClassSubDivision): self
    {
        $this->ClassSubDivision = $ClassSubDivision;

        return $this;
    }

    /**
     * @return Collection|TestMarkSheet[]
     */
    public function getTestMarkSheets(): Collection
    {
        return $this->testMarkSheets;
    }

    public function addTestMarkSheet(TestMarkSheet $testMarkSheet): self
    {
        if (!$this->testMarkSheets->contains($testMarkSheet)) {
            $this->testMarkSheets[] = $testMarkSheet;
            $testMarkSheet->setStudent($this);
        }

        return $this;
    }

    public function removeTestMarkSheet(TestMarkSheet $testMarkSheet): self
    {
        if ($this->testMarkSheets->contains($testMarkSheet)) {
            $this->testMarkSheets->removeElement($testMarkSheet);
            // set the owning side to null (unless already changed)
            if ($testMarkSheet->getStudent() === $this) {
                $testMarkSheet->setStudent(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?StudentStatus
    {
        return $this->status;
    }

    public function setStatus(?StudentStatus $status): self
    {
        $this->status = $status;

        return $this;
    }


}
