<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClassSubDivisionListRepository")
 */
class ClassSubDivisionList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Student", mappedBy="ClassSubDivision",cascade={"persist","remove"})
     */
    private $students;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TestMarkSheet", mappedBy="subdivision")
     */
    private $testMarkSheets;

    public function __construct()
    {
        $this->students = new ArrayCollection();
        $this->testMarkSheets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setClassSubDivisionList($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {

        if ($this->student->contains($student)) {
            $this->student->removeElement($student);
            $student->removeClassSubDivisionList(null);
        }
        return $this;
    }

    /**
     * @return Collection|TestMarkSheet[]
     */
    public function getTestMarkSheets(): Collection
    {
        return $this->testMarkSheets;
    }

    public function addTestMarkSheet(TestMarkSheet $testMarkSheet): self
    {
        if (!$this->testMarkSheets->contains($testMarkSheet)) {
            $this->testMarkSheets[] = $testMarkSheet;
            $testMarkSheet->setSubdivision($this);
        }

        return $this;
    }

    public function removeTestMarkSheet(TestMarkSheet $testMarkSheet): self
    {
        if ($this->testMarkSheets->contains($testMarkSheet)) {
            $this->testMarkSheets->removeElement($testMarkSheet);
            // set the owning side to null (unless already changed)
            if ($testMarkSheet->getSubdivision() === $this) {
                $testMarkSheet->setSubdivision(null);
            }
        }

        return $this;
    }

}
