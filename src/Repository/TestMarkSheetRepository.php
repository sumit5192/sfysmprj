<?php

namespace App\Repository;

use App\Entity\TestMarkSheet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TestMarkSheet|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestMarkSheet|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestMarkSheet[]    findAll()
 * @method TestMarkSheet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestMarkSheetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestMarkSheet::class);
    }

    // /**
    //  * @return TestMarkSheet[] Returns an array of TestMarkSheet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TestMarkSheet
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
