<?php

namespace App\Repository;

use App\Entity\ClassSubDivisionList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ClassSubDivisionList|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClassSubDivisionList|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClassSubDivisionList[]    findAll()
 * @method ClassSubDivisionList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClassSubDivisionListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClassSubDivisionList::class);
    }

    // /**
    //  * @return ClassSubDivisionList[] Returns an array of ClassSubDivisionList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClassSubDivisionList
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
