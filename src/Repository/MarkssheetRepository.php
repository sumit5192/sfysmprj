<?php

namespace App\Repository;

use App\Entity\Markssheet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Markssheet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Markssheet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Markssheet[]    findAll()
 * @method Markssheet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MarkssheetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Markssheet::class);
    }

    // /**
    //  * @return Markssheet[] Returns an array of Markssheet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Markssheet
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
