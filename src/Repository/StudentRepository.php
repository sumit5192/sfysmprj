<?php

namespace App\Repository;

use App\Entity\Student;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Student|null find($id, $lockMode = null, $lockVersion = null)
 * @method Student|null findOneBy(array $criteria, array $orderBy = null)
 * @method Student[]
 * @method Student[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Student::class);
    }


    public function  findSortingAll()
    {
        return $this->createQueryBuilder('s')
                    ->andWhere('s.deletedAt IS NULL') //is  null
                    ->getQuery();
    }

    public function findDeletedRecords()
    {
        return $this->createQueryBuilder('s')
                    ->Where('s.deletedAt IS NOT NULL') //is  null
                    ->getQuery()
                    ->getResult();
    }

    public function getstudentlist()
    {
        return $this->createQueryBuilder('s')
                     ->getQuery()
                     ->getArrayResult();
    }

    public function findfilterStudentlist($searchTerm)
    {
        return $this->createQueryBuilder('s')
                        ->leftJoin('s.remarks', 'r')
                        ->where('s.email like :searchTerm')
                        ->orWhere('r.remarks LIKE :searchTerm')
                        ->orWhere('s.mobile LIKE :searchTerm')
                        ->orWhere('s.name LIKE :searchTerm')
                        ->setParameter('searchTerm', '%'.$searchTerm.'%')

            ;
    }

    public function restorestudent($studentid)
    {

        return $qb = $this->createQueryBuilder('s')
                    ->update('App\Entity\Student','s')
                    ->andWhere('s.id = :studentid')
                    ->setParameter('studentid', $studentid)
                    ->set('s.deletedAt',NULL)
                    ->getQuery()
                    ->execute();

    }

    public function filterbyClassSectionandDivision($classId,$divisionID)
    {
        return $qb = $this->createQueryBuilder('s')
            ->andWhere('s.ClassSection = :classId')
            ->andWhere('s.ClassSubDivision = :divisionID')
            ->setParameter('classId', $classId)
            ->setParameter('divisionID', $divisionID)
            ->getQuery()
            ->getArrayResult();
    }
    // /**
    //  * @return Student[] Returns an array of Student objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Student
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
