<?php

namespace App\Repository;

use App\Entity\ClassSectionSubDivision;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ClassSectionSubDivision|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClassSectionSubDivision|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClassSectionSubDivision[]    findAll()
 * @method ClassSectionSubDivision[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClassSectionSubDivisionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClassSectionSubDivision::class);
    }

    // /**
    //  * @return ClassSectionSubDivision[] Returns an array of ClassSectionSubDivision objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClassSectionSubDivision
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
