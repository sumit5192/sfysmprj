<?php

namespace App\Repository;

use App\Entity\ClassSection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ClassSection|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClassSection|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClassSection[]    findAll()
 * @method ClassSection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClassSectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClassSection::class);
    }

    // /**
    //  * @return ClassSection[] Returns an array of ClassSection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClassSection
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
