<?php

namespace App\Repository;

use App\Entity\SubjectList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SubjectList|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubjectList|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubjectList[]    findAll()
 * @method SubjectList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubjectListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubjectList::class);
    }

    // /**
    //  * @return SubjectList[] Returns an array of SubjectList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SubjectList
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
