<?php

namespace App\Repository;

use App\Entity\Studentremarks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Studentremarks|null find($id, $lockMode = null, $lockVersion = null)
 * @method Studentremarks|null findOneBy(array $criteria, array $orderBy = null)
 * @method Studentremarks[]    findAll()
 * @method Studentremarks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentremarksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Studentremarks::class);
    }

    public function findByStudentid($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.student = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Studentremarks[] Returns an array of Studentremarks objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Studentremarks
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
