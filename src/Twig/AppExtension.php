<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 20/9/19
 * Time: 12:17 PM
 */

namespace App\Twig;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * @Annotation
 */
class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('Uppercase', [$this, 'ConvertUppercase']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('AppendNewText', [$this, 'AppendText']),
        ];
    }

    public function ConvertUppercase($string)
    {

        return strtoupper($string);
    }
    public function AppendText($string,$appendVal)
    {

        return $string.' '. $appendVal;
    }
}