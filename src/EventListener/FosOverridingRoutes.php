<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 13/9/19
 * Time: 11:08 AM
 */

namespace App\EventListener;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Annotation\Route;

class FosOverridingRoutes implements EventSubscriberInterface
{
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return [
              FOSUserEvents::REGISTRATION_CONFIRMED =>'onRegistrationSuccess',
        ];
    }



    public function onRegistrationSuccess(FilterUserResponseEvent $event)
    {

        //$url = $this->router->generate('student.index');
        //$event->setResponse(new RedirectResponse($url));
        //dd($event);
    }

}