<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 1/10/19
 * Time: 9:54 AM
 */

namespace App\EventListener;



use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Doctrine\ORM\EntityManager;




class AuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    protected $entityManager;

    /**
     * LoginSuccessHandler constructor.
     *
     * @param Router $router
     * @param AuthorizationChecker|AuthorizationCheckerInterface $authorizationChecker
     * @param EntityManager $entityManager
     */
    public function __construct(Router $router, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $token = $event->getAuthenticationToken();
        $request = $event->getRequest();
        $this->onAuthenticationSuccess($request, $token);
    }
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {

        //dump($username =$request->get('_username'));
        /*$user = $token->getUser();
        dump($user->getroles());
        dump($this->authorizationChecker->isGranted('ROLE_ADMIN'));*/
        if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
        {
            return new RedirectResponse('admin');
        }
        else if($this->authorizationChecker->isGranted('ROLE_STUDENT'))
        {
            return new RedirectResponse('student');
        }
        else
        {
            return new RedirectResponse('student');
        }

    }
}
