<?php

namespace App\Form;

use App\Entity\Student;
use App\Entity\Studentremarks;
use App\Repository\StudentRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentremarksFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('remarks',TextareaType::class)
            ->add('student',EntityType::class,[
                'class' => Student::class,
                'choice_label' => 'name',
                'query_builder' => function(StudentRepository $studentRepository)
                {
                    return $studentRepository->createQueryBuilder('s')
                                             ->andWhere('s.deletedAt IS NULL')
                                             ->distinct();
                }
                ])
            ->add('Save',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Studentremarks::class,
        ]);
    }
}
