<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 13/4/20
 * Time: 5:36 PM
 */

namespace App\Form;

use App\Entity\ClassSection;
use App\Entity\ClassSubDivisionList;
use App\Entity\Student;
use App\Entity\Studentremarks;
use App\Entity\TestMarkSheet;
use App\Repository\ClassSectionRepository;
use App\Repository\StudentRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TestMarksType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name',TextType::class)
            ->add('date',DateType::class)
            ->add('ClassSection',EntityType::class,[
                'class' => ClassSection::class,
                'choice_label' => 'name',
                'query_builder' => function(ClassSectionRepository $classSectionRepository)
                {
                    return $classSectionRepository->createQueryBuilder('c')
                        ->distinct();
                },
                'required' => true
            ])
            ->add('subdivision',EntityType::class,[
                'class' => ClassSubDivisionList::class,
                'choice_label' => 'name',
                'query_builder' => function($classSectionRepository)
                {
                    return $classSectionRepository->createQueryBuilder('c')
                        ->distinct();
                },
                'required' => true
            ])
            ->add('Student',EntityType::class,[
                'class' => Student::class,
                'choice_label' => 'name',
                'query_builder' => function(StudentRepository $studentRepository)
                {
                    return $studentRepository->createQueryBuilder('s')
                        ->distinct();
                },
                'required' => true
            ])
            ->add('Save',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TestMarkSheet::class,
        ]);
    }
}
