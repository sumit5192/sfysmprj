<?php
namespace App\Form;


use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
class FacultyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname',TextType::class)
           ->add('roles',ChoiceType::class,[
                'choices' => [
                    'Admin' => 'ROLE_ADMIN',
                    'Student'=>'ROLE_STUDENT',
                    'Faculty' => 'ROLE_FACULTY'
                ],
                'label' => 'select Role',
                'placeholder' =>'Select Role',
                'required' =>true,
                'multiple' =>false,
                'expanded' =>false,
                'mapped' => false
            ])
            ->add('mobile',\Symfony\Component\Form\Extension\Core\Type\IntegerType::class,[
                'required' => true
            ])
            ->add('address',TextareaType::class,[
                'required' => true
            ])
            ->add('qualification',TextType::class,[
                'required' => true
            ])

        ;

    }
    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }
}