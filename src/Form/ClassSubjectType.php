<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 17/3/20
 * Time: 12:14 PM
 */

namespace App\Form;
use App\Entity\ClassSection;
use App\Entity\ClassSubject;
use App\Entity\SetSubjectEntity;
use App\Entity\SubjectList;
use App\Repository\ClassSubjectRepository;
use App\Repository\SubjectListRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ClassSubjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           // ->add('name',TextType::Class);
        ->add('subject',EntityType::class,[
        'class' => SubjectList::class,
        'choice_label' => 'name',
        'query_builder' => function(SubjectListRepository $classSubject)
        {
            return $classSubject->createQueryBuilder('s')
                ;
        },
        'required' => true
    ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ClassSubject::class,
        ]);
    }
}