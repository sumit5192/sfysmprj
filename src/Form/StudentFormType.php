<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 17/9/19
 * Time: 12:26 PM
 */

namespace App\Form;


use App\Entity\Cast;
use App\Entity\ClassSection;
use App\Entity\ClassSectionSubDivision;
use App\Entity\ClassSubDivisionList;
use App\Entity\Student;
use App\Entity\StudentStatus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Doctrine\Persistence\ObjectManager;

class StudentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        //$entityManager = $options['entity_manager'];

        $builder
            ->add('name',TextType::class)
            ->add('email',TextType::class)
            ->add('mobile',IntegerType::class)
            ->add('cast',EntityType::class, [
                'class' => Cast::class,
                'choice_label' => 'name',

            ])
            ->add('hobbies',CollectionType::class,[
                'entry_type' => HobbieFormType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
                'entry_options' => [
                    'label' => false
                ]
            ])
            ->add('ClassSection',EntityType::class, [
                'class' => ClassSection::class,

                'choice_label' => function ($classSection) {
                    return $classSection->getName();
                },
                'required' => true
            ])
            ->add('ClassSubDivision',EntityType::class, [
                'class' => ClassSubDivisionList::class,

                'choice_label' => function ($classSection) {
                    return $classSection->getName();
                },
                'required' => true
            ])
            ->add('bithdate',DateType::class)
            ->add('favourite_color',TextType::class)
            ->add('status',EntityType::class, [
                'class' => StudentStatus::class,

                'choice_label' => function ($classSection) {
                    return $classSection->getName();
                },
                'required' => true
            ])

            ->add('Save',SubmitType::class,
                [
                    'attr' =>[
                        'class' => ''
                    ]
                ])
           /* ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event){
                $eventData = $event->getData();
                $formData = $event->getForm();
                $formData->add('email',TextType::class, [
                    'data' =>'ss'
                ]);
//                if ($eventData->getMobile() === null) {
//                    $event->getForm()->remove('mobile');
//                }
//                $formData->add('cast',ChoiceType::class, array(
//                    'choices' => array(
//                        '1' => 'A',
//                        '2' => 'B',
//                        '3' => 'C',
//                    ),
//                    'required' => true
//                ));
                //$event->setData($formData);
                //dump($formData);
            })*/

//            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event){
//                $eventData = $event->getData();
//                $formData = $event->getForm();
//                $formData->add('ClassSubDivision', TextType::class, ['mapped' => true]);
//            });
        ;

        $builder->get('favourite_color')
                    ->addModelTransformer(new CallbackTransformer(
                        function ($tagsAsArray) {
                            // transform the array to a string
                            // Display time convert array to string

                            if(!empty(unserialize($tagsAsArray)))
                            {
                                // not null
                                return implode(',',unserialize($tagsAsArray));
                            }
                            return '';
                        },
                        function ($tagsAsString) {
                            // transform the string back to an array
                            // submit type convert to string to array
                            return serialize(explode(',', $tagsAsString));
                        }
                    ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Student::class,
            'entity_manager' => null
        ]);
    }

    public function getClassDivision($em,$id)
    {

       // $classDivisionData = $em->getRepository(ClassSubDivisionList::class)->find($id);
        $classDivisionData = $id;
        if($classDivisionData !== null)
        {
            return [ $classDivisionData->getName() => $classDivisionData->getId() ];
        }
       return [];

    }


}