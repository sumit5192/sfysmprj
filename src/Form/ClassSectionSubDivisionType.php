<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 13/3/20
 * Time: 2:22 PM
 */

namespace App\Form;


use App\Entity\ClassSectionSubDivision;
use App\Entity\ClassSubDivisionList;
use App\Repository\ClassSubDivisionListRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClassSectionSubDivisionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           // ->add('name',TextType::Class);
           ->add('division',EntityType::class,[
               'class' => ClassSubDivisionList::class,
               'choice_label' => 'name',
               'query_builder' => function(ClassSubDivisionListRepository $classDivision)
               {
                   return $classDivision->createQueryBuilder('c')
                       ;
               },
               'required' => true
           ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ClassSectionSubDivision::class,
        ]);
    }
}