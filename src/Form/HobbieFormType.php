<?php

namespace App\Form;

use App\Entity\Hobbie;
use App\validator\Constraints\CheckEntityHobbie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class HobbieFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
//            ->add('descriptions',TextareaType::class,[
//                'constraints' => new CheckEntityHobbie()
//            ])
            ->add('descriptions',TextareaType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Hobbie::class,
        ]);
    }
}
