<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 24/10/19
 * Time: 4:19 PM
 */

namespace App\Controller;
use App\Entity\ClassSection;
use App\Entity\ClassSubject;
use App\Entity\Hobbie;
use App\Entity\Student;
use App\Entity\Studentremarks;
use App\Entity\SubjectList;
use App\Entity\User;
use App\Form\ClassSectionType;
use App\Form\StudentFormType;
use App\Repository\StudentremarksRepository;
use App\Repository\StudentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin.")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="index",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        $total_Student = count($em->getRepository(Student::class)->findAll());
        $total_classSection= count($em->getRepository(ClassSection::class)->findAll());
        $total_faculty = count($em->getRepository(User::class)->getFacultyUser());
        $total_admin= count($em->getRepository(User::class)->getAdminUser());
        return $this->render('admin/dashboard.html.twig',[
            'total_student' => $total_Student,
            'total_classSection' => $total_classSection,
            'total_faculty' => $total_faculty,
            'total_admin' => $total_admin
        ]);
    }

    /**
     * @Route("/deleted/student", name="deleted.student",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function DeletedStudentList(Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        $DeletedStudentList = $em->getRepository(Student::class)->findDeletedRecords();

        return $this->render('admin/student/list.html.twig', [
            'studentlist' => $DeletedStudentList
        ]);

    }

    /**
     * @Route("/restore/student/{id}", name="restore.student",methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function RestoredStudentList(Request $request,Student $student)
    {

        $em= $this->getDoctrine()->getManager();
        $student->setDeletedAt(NULL);
        $em->persist($student);
        $em->flush();
        $student->setDeletedAt(NULL);
        $this->addFlash('success', 'Record succesfully Restore.');
        return $this->redirectToRoute('admin.deleted.student');

        /*$remarksList = $em->getRepository(Studentremarks::class)->findBy(array('student' => $student->getId()));

        if(!empty($remarks)) {
            foreach ($remarksList as $remarks)
            {

                $remarks->setDeletedAt(NULL);
                dump($remarks);
                $em->persist($remarks);
            }


        }*/


    }

    /**
     * @Route("/class", name="classIndex",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function classes(Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        $classSectionList =  $em->getRepository(ClassSection::class)->findAll();
        $knp_list = $em->getRepository(ClassSection::class)->findAll();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $knp_list, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            5/*limit per page*/
        );


        return $this->render('admin/classSection/list.html.twig',[
            'classSectionList' => $classSectionList,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/class/add/{id}", name="classAdd",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addclasses($id = 0 ,Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        if($id)
        {
            $classSection = $em->getRepository(ClassSection::class)->find($id);
        }
        else{
            $classSection = new ClassSection();
        }

        $form =$this->createForm(ClassSectionType::class,$classSection);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {

            $em->persist($classSection);
            $em->flush();
            $this->addFlash('success','Record succesfully Saved.');
            return $this->redirectToRoute('admin.classIndex');
        }
        return $this->render('admin/classSection/create.html.twig',[
            'formdata' =>$form->createView(),
            'classSection' => $classSection
        ]);
    }

    /**
     * @Route("/class/delete/{id}", name="classDelete",methods={"GET","POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteclasses(ClassSection $classSection)
    {
        $em= $this->getDoctrine()->getManager();

        foreach ($classSection->getStudents() as $data)
        {
            $data->setClassSection(null);
            $data->setClassSubDivision(null);

        }
        $em->persist($classSection);
        $em->flush();
        $em->remove($classSection);
        $em->flush();

        $this->addFlash('success','Record succesfully Deleted.');
        return $this->redirectToRoute('admin.classIndex');
    }



}