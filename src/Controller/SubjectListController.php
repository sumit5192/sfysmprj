<?php

namespace App\Controller;

use App\Entity\SubjectList;
use App\Form\SubjectListType;
use App\Repository\SubjectListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/subject/list")
 */
class SubjectListController extends AbstractController
{
    /**
     * @Route("/", name="subject_list_index", methods={"GET"})
     */
    public function index(SubjectListRepository $subjectListRepository): Response
    {
        return $this->render('admin/subject_list/index.html.twig', [
            'subject_lists' => $subjectListRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="subject_list_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $subjectList = new SubjectList();
        $form = $this->createForm(SubjectListType::class, $subjectList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($subjectList);
            $entityManager->flush();

            return $this->redirectToRoute('subject_list_index');
        }

        return $this->render('admin/subject_list/new.html.twig', [
            'subject_list' => $subjectList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="subject_list_show", methods={"GET"})
     */
    public function show(SubjectList $subjectList): Response
    {
        return $this->render('admin/subject_list/show.html.twig', [
            'subject_list' => $subjectList,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="subject_list_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SubjectList $subjectList): Response
    {
        $form = $this->createForm(SubjectListType::class, $subjectList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('subject_list_index');
        }

        return $this->render('admin/subject_list/edit.html.twig', [
            'subject_list' => $subjectList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="subject_list_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SubjectList $subjectList): Response
    {
        if ($this->isCsrfTokenValid('delete'.$subjectList->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($subjectList);
            $entityManager->flush();
        }

        return $this->redirectToRoute('subject_list_index');
    }
}
