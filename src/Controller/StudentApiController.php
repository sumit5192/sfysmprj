<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 20/9/19
 * Time: 5:49 PM
 */

namespace App\Controller;
use App\Entity\JwtRefreshToken;
use App\Entity\Student;
use App\Entity\User;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("api/",name="api_")
 */
class StudentApiController extends Controller
{



    /**
     * @Route(path="student/list",name="student_lists")
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function getlist()
    {
       $list = $this->getDoctrine()->getRepository(Student::class)->getstudentlist();
        //dump($list);
        return new JsonResponse($list);
    }
    /**
    * @Route(path="login_check",name="login_check",methods={"GET","POST"})
    * @return \Symfony\Component\HttpFoundation\RedirectResponse
    */
    public function logincheck(Request $request,JWTTokenManagerInterface $JWTTokenManager)
    {
        //return new JsonResponse(['Error' => 'Invalid Credentials']);
        $username =  $request->request->get('username');
        $password =  $request->request->get('password');

        if(empty($username) || empty($password))
        {
            return new JsonResponse(['Error' => 'Invalid Credentials']);
        }
        //$user = new User();

        //$password = password_hash(trim($password), PASSWORD_ARGON2I);
        //dd($password);
        $em= $this->getDoctrine();
        $user = $em->getRepository(User::class)->findOneBy(array('username' => $username,'password'=>$password));
        if($user === null)
        {
            return new JsonResponse(['Error' => 'Invalid Credentials']);
        }

        $token = $JWTTokenManager->create($user);

        $this->createCookie($token);
        return new JsonResponse(['token' => $token]);
    }


    /*public function tokenBlackist(Request $request,JWTAuthenticatedEvent $authenticatedEvent)
    {
        //return new JsonResponse(['Error' => 'Invalid Credentials']);
        $username =  $request->request->get('username');
        $password =  $request->request->get('password');

        if(empty($username) || empty($password))
        {
            return new JsonResponse(['Error' => 'Invalid Credentials']);
        }
        //$user = new User();

        //$password = password_hash(trim($password), PASSWORD_ARGON2I);
        //dd($password);
        $em= $this->getDoctrine();
        $user = $em->getRepository(User::class)->findOneBy(array('username' => $username,'password'=>$password));
        if($user === null)
        {
            return new JsonResponse(['Error' => 'Invalid Credentials']);
        }

        $token = $JWTTokenManager->create($user);
        refresh_tokens
        $dd = new RefreshToken();
        $dd->setRefreshToken()

        $this->createCookie($token);
        return new JsonResponse(['token' => $token]);
    }*/
    protected function createCookie($jwt)
    {

        $response = new Response();
        $response->headers->setCookie(
            new Cookie(
                "Bearer ".$jwt,
                $jwt,
                new \DateTime("+1 day"),
                "/",
                null,
                false,
                true,
                false,
                'strict'
            )
        );
        return $response;
    }
}

/*
 *  Url refrence:
 * https://emirkarsiyakali.com/implementing-jwt-authentication-to-your-api-platform-application-885f014d3358
 * https://www.youtube.com/watch?v=XT4oy1d1j-g
 * https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/1-configuration-reference.md#full-default-configuration
 * pass token : Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1ODI2MzU2MjYsImV4cCI6MTU4MjY0MjgyNiwicm9sZXMiOlsiUk9MRV9BTExPV0VEX1RPX1NXSVRDSCIsIlJPTEVfVVNFUiJdLCJ1c2VybmFtZSI6InN1bWl0In0.iRAORR6tA5vDhrHaRWaMiv2Wj77ytm9Qx8_OpnbcktYp_ygye7dbj-pQp_9-x3MEOw09LEr0D6iBJYDM53fsTQtlOOWbs3f_EWbsE5JZXd7HUpO1FJ2BuP_u8yjdZbLTkxVoOAIiKxROnytjfNrb7eYY4j1zGi-99j86FpTVJ5rOYbrVXRl9b19i6f0x8Isxrn05yqebFtNaZ_k2-mrxzszWNFrBEZWid_xTAlGMrbnEB8fVSt8nPr6JTkcnyVU2_Czd5i5dhxGf-KQzzwmzbhEmZeO1GnNJWWNbor_fmZiSJ7kr9tKvn6TrN-VcNQq0J70a6vX2QFmWmSJxC1T1YbsCaBp4Yzzlb7Hr83dZl0AgTPFOfCcqV5Kfyske52xwFPi8iQmBBXssLALrgbz6BUv2Ai2Ft1eFMQzv2_y3CUnglmroox-nzgx90A-PoMrknwb_P8eUNZfj11WUhVY98ffxwzS3GMyQvyDCUrcw9u5ONHyJTlFF4ugxeQqvRuLtUQK9KdUoIvMWbn4fcCfHlHT10ZkYqtxdCJ2X92IgOATRfLA4nfbH_u-XBpX_QwpZeD4Y-unLZZPf1S8korB7zPFEJE4iG8bUAQ6Fs1T1Mq-OlHGPWrscaQWcu3V3ZADH8vHdDkzS_0MjyZ25XAJCi7g__S3PIvDTfbR4zzsMcQU
 *  Refresh token : https://github.com/markitosgv/JWTRefreshTokenBundle#revoke-a-token
*/