<?php

namespace App\Controller;

use App\Entity\StudentStatus;
use App\Form\StudentStatusType;
use App\Repository\StudentStatusRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/student/status")
 */
class StudentStatusController extends AbstractController
{
    /**
     * @Route("/", name="student_status_index", methods={"GET"})
     */
    public function index(StudentStatusRepository $studentStatusRepository): Response
    {
        return $this->render('admin/student_status/index.html.twig', [
            'student_statuses' => $studentStatusRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="student_status_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $studentStatus = new StudentStatus();
        $form = $this->createForm(StudentStatusType::class, $studentStatus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($studentStatus);
            $entityManager->flush();

            return $this->redirectToRoute('student_status_index');
        }

        return $this->render('admin/student_status/new.html.twig', [
            'student_status' => $studentStatus,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="student_status_show", methods={"GET"})
     */
    public function show(StudentStatus $studentStatus): Response
    {
        return $this->render('admin/student_status/show.html.twig', [
            'student_status' => $studentStatus,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="student_status_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, StudentStatus $studentStatus): Response
    {
        $form = $this->createForm(StudentStatusType::class, $studentStatus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('student_status_index');
        }

        return $this->render('admin/student_status/edit.html.twig', [
            'student_status' => $studentStatus,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="student_status_delete", methods={"DELETE"})
     */
    public function delete(Request $request, StudentStatus $studentStatus): Response
    {
        if ($this->isCsrfTokenValid('delete'.$studentStatus->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($studentStatus);
            $entityManager->flush();
        }

        return $this->redirectToRoute('student_status_index');
    }
}
