<?php

namespace App\Controller;

use App\Entity\ClassSubDivisionList;
use App\Form\ClassSubDivisionListType;
use App\Repository\ClassSubDivisionListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/class/sub/division/list")
 */
class ClassSubDivisionListController extends AbstractController
{
    /**
     * @Route("/", name="class_sub_division_list_index", methods={"GET"})
     */
    public function index(ClassSubDivisionListRepository $classSubDivisionListRepository): Response
    {
        return $this->render('admin/class_sub_division_list/index.html.twig', [
            'class_sub_division_lists' => $classSubDivisionListRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="class_sub_division_list_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $classSubDivisionList = new ClassSubDivisionList();
        $form = $this->createForm(ClassSubDivisionListType::class, $classSubDivisionList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($classSubDivisionList);
            $entityManager->flush();

            return $this->redirectToRoute('class_sub_division_list_index');
        }

        return $this->render('admin/class_sub_division_list/new.html.twig', [
            'class_sub_division_list' => $classSubDivisionList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="class_sub_division_list_show", methods={"GET"})
     */
    public function show(ClassSubDivisionList $classSubDivisionList): Response
    {
        return $this->render('admin/class_sub_division_list/show.html.twig', [
            'class_sub_division_list' => $classSubDivisionList,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="class_sub_division_list_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ClassSubDivisionList $classSubDivisionList): Response
    {
        $form = $this->createForm(ClassSubDivisionListType::class, $classSubDivisionList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('class_sub_division_list_index');
        }

        return $this->render('admin/class_sub_division_list/edit.html.twig', [
            'class_sub_division_list' => $classSubDivisionList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="class_sub_division_list_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ClassSubDivisionList $classSubDivisionList): Response
    {
        if ($this->isCsrfTokenValid('delete'.$classSubDivisionList->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($classSubDivisionList);
            $entityManager->flush();
        }

        return $this->redirectToRoute('class_sub_division_list_index');
    }
}
