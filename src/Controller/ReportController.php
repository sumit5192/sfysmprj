<?php
namespace App\Controller;
use App\Entity\ClassSection;
use App\Entity\ClassSectionSubDivision;
use App\Entity\ClassSubDivisionList;
use App\Entity\SubjectList;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/report", name="admin_report.")
 */

class ReportController extends Controller
{
    /**
     * @Route("/SC-graph", name="SC_report_graph",methods={"GET","POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reportGraph()
    {
        $em= $this->getDoctrine()->getManager();
        $classSectionList =  $em->getRepository(ClassSection::class)->findAll();

        $StudentClassPieChart = [];
        foreach ($classSectionList as $classSection)
        {
            $StudentClassPieChart[] = ['classname' => $classSection->getName(), 'count' =>count($classSection->getStudents())];
        }
        $SubjectSectionList =  $em->getRepository(SubjectList::class)->findAll();
        foreach ($SubjectSectionList as $subjectSection)
        {
            $SubjectClassPieChart[] = ['classname' => $subjectSection->getName(), 'count' =>count($subjectSection->getClassSections())];

        }
        $SubDivisonSectionList =  $em->getRepository(ClassSubDivisionList::class)->findAll();

        foreach ($SubDivisonSectionList as $subDivisionSection)
        {
            $SubDivisionClassPieChart[] = ['name' => $subDivisionSection->getName(), 'count' =>count($em->getRepository(ClassSectionSubDivision::class)->findBy(['division' => $subDivisionSection->getId()]))];

        }

        return $this->render('admin/report/report_graph_class.html.twig',[
            'StudentClassPieChart' => $StudentClassPieChart,
            'SubjectClassPieChart' => $SubjectClassPieChart,
            'SubDivisionClassPieChart' => $SubDivisionClassPieChart,
        ]);
    }
}