<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 24/3/20
 * Time: 10:18 AM
 */

namespace App\Controller;


use App\Entity\Faculty;
use App\Entity\User;
use App\Form\FacultyType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/faculty", name="faculty.")
 */
class FacultyController extends Controller
{
    /**
     * @Route("/", name="index",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        $facultyData = $em->getRepository(User::class)->getFacultyUser();
        return $this->render('admin/faculty/list.html.twig',
            [
                'facultyData' => $facultyData
            ]);
    }

    /**
     * @Route("/create/{id}", name="create",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create($id =0,Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        if($id)
        {
            $faculty = $em->getRepository(User::class)->find($id);
        }
        else{
            $faculty = new User();
        }

        $form =$this->createForm(FacultyType::class,$faculty);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $getrole =$request->request->get('faculty')['roles'];
            $faculty->addRole($getrole);
            $faculty->setEnabled(1);
            $em->persist($faculty);
            $em->flush();
            $this->addFlash('success','Record succesfully Saved.');
            return $this->redirectToRoute('faculty.index');
        }
        return $this->render('admin/faculty/create.html.twig',[
            'form' =>$form->createView(),
            'faculty' => $faculty
        ]);
    }

    /**
     * @Route(path="/delete/{id}",name="delete")
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(User $user)
    {
        $em= $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash('success','Record succesfully Deleted.');
        return $this->redirectToRoute('faculty.index');
    }
}