<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 13/4/20
 * Time: 4:59 PM
 */

namespace App\Controller;

use App\Entity\ClassSection;
use App\Entity\ClassSubject;
use App\Entity\Hobbie;
use App\Entity\Markssheet;
use App\Entity\Student;
use App\Entity\Studentremarks;
use App\Entity\SubjectList;
use App\Entity\TestMarkSheet;
use App\Form\StudentFormType;
use App\Form\TestMarksType;
use App\Repository\StudentremarksRepository;
use App\Repository\StudentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/marks", name="marks.")

 */
class TestMarksController extends Controller
{
    /**
     * @Route("/", name="index",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $em= $this->getDoctrine();
        $knp_list = $em->getRepository(TestMarkSheet::class)->findAll();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $knp_list, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            5/*limit per page*/
        );
        return $this->render('admin/testMarks/list.html.twig', [
            'pagination' => $pagination,

        ]);


        return $this->render('admin/testMarks/list.html.twig');
    }

    /**
     * @Route("/create/{id}", name="create",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create($id =0,Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        if($id)
        {
            $TestMarkSheet = $em->getRepository(TestMarkSheet::class)->find($id);
        }
        else{
            $TestMarkSheet = new TestMarkSheet();
        }

        $form =$this->createForm(TestMarksType::class,$TestMarkSheet);
        $form->handleRequest($request);
        $GetmarksheetData = [];
        if(!empty($TestMarkSheet->getMarkssheets()))
        {
            $i =1;
            foreach ($TestMarkSheet->getMarkssheets() as $marks)
            {
                $GetmarksheetData[$i]['marks'] = $marks->getMarks();
                $GetmarksheetData[$i]['SubjectID'] = $marks->getSubject()->getValues()[0]->getId();
                $GetmarksheetData[$i]['SubjectName'] = $marks->getSubject()->getValues()[0]->getName();
                $GetmarksheetData[$i]['id'] = $marks->getId();
                $i++;
            }

        }

        if($form->isSubmitted() && $form->isValid())
        {
            if(!empty($request->request->get('marksval')))
            {
                //dd($request->request);
                foreach ($request->request->get('marksval') as $key=>$val)
                {

                    if(empty($val['id']))
                    {
                        foreach ($TestMarkSheet->getMarkssheets() as $data)
                        {
                            $TestMarkSheet->removeMarkssheet($data);
                        }
                        $em->persist($TestMarkSheet);
                        $em->flush();
                        $marksheet = new Markssheet();
                    }
                    else{
                        $marksheet = $em->getRepository(Markssheet::class)->find($val['id']);

                    }

                    $marksheet->setMarks($val['marks']);
                    $marksheet->addTest($TestMarkSheet);
                    $marksheet->addSubject($em->getRepository(SubjectList::class)->find($key));
                    $em->persist($marksheet);
                }
            }

            $em->persist($TestMarkSheet);
            $em->flush();
            $this->addFlash('success','Record succesfully Saved.');
            return $this->redirectToRoute('marks.index');
        }
        return $this->render('admin/testMarks/create.html.twig',[
            'formdata' =>$form->createView(),
            'TestMarkSheet' => $TestMarkSheet,
            'MarksData' => $GetmarksheetData
        ]);
    }

    /**
     * @Route(path="/delete/{id}",name="delete")
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(TestMarkSheet $testMarkSheet)
    {
        $em= $this->getDoctrine()->getManager();
        $em->remove($testMarkSheet);
        $em->flush();

        $this->addFlash('success','Record succesfully Deleted.');
        return $this->redirectToRoute('marks.index');
    }

    /**
     * @Route(path="/ajax/getsubjectlistBYClassId",name="ajax_subjectListBYClassID")
     * @param Student $student
     * @return JsonResponse
     */
    public function ajaxSubjectListByClassId(Request $request)
    {
        $classID = $request->get('classID');
        $ResponceData = $ResponseSubjectData = [];
        if($classID) {
            $em= $this->getDoctrine()->getManager();
            $classSection = $em->getRepository(ClassSection::class)->find($classID);

            foreach ($classSection->getClassSectionSubDivisions() as $data)
            {
                $ResponceData[$data->getDivision()->getId()] = $data->getDivision()->getName();
            }


            foreach ($classSection->getClassSubjects() as $subject)
            {
                $ResponseSubjectData[$subject->getSubject()->getId()] = $subject->getSubject()->getName();
            }
        }

        return new JsonResponse([
            'success' => false,
            'error' => false,
            'data' => [$ResponceData],
            'SubjectData' => [$ResponseSubjectData]
        ]);
    }
    /**
     * @Route(path="/ajax/getstudentlistBYClassId",name="ajax_studentListBYClassID")
     * @param Student $student
     * @return JsonResponse
     */
    public function ajaxStudentListByClassId(Request $request)
    {
        $classID = $request->get('classID');
        $classsubdivisionID = $request->get('classsubdivisionID');
        $ResponceData = $ResponseSubjectData = [];
        if($classID && $classsubdivisionID) {
            $em= $this->getDoctrine()->getManager();
            $studentData = $em->getRepository(Student::class)->findBy(array('ClassSection' => $classID,'ClassSubDivision' => $classsubdivisionID));

            foreach ($studentData as $data)
            {
                $ResponceData[$data->getId()] = $data->getName();
            }
        }

        return new JsonResponse([
            'success' => false,
            'error' => false,
            'data' => [$ResponceData]
        ]);
    }


}