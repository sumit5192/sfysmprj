<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 23/3/20
 * Time: 2:48 PM
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route("/", name="rootIndex",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
       return $this->redirectToRoute('fos_user_security_login');
    }
}