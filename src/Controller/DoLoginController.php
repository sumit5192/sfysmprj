<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpClientKernel;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;

/**
 * @Route("/dologin", name="dologin.")
 */
class DoLoginController extends Controller
{
    /**
     * @Route("/", name="index",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {

        /*$formData = [
            'username' =>'sumit',
            'password' => '$argon2i$v=19$m=65536,t=4,p=1$by9jSzQ1bnlPVGhkdlRNdA$PPZyTGw+XTRPCGscY5xTif+PlaZ+6UGBGy1HOETgXhI'
        ];

        $client = new CurlHttpClient();
        $response = $client->request('GET', 'http://127.0.0.1:8001/api/login_check');

        $statusCode = $response->getStatusCode();
        if($statusCode == 200)
        {
            $decodedPayload = $response->toArray();
            dd($decodedPayload);
        }*/
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1ODI4ODEyNDEsImV4cCI6MTU4Mjg4ODQ0MSwicm9sZXMiOlsiUk9MRV9BTExPV0VEX1RPX1NXSVRDSCIsIlJPTEVfVVNFUiJdLCJ1c2VybmFtZSI6InN1bWl0In0.hjLc6XeJLrfyOqJ7imeVR5AwfUSckucjg6WOpvwb9V3YCNfqYucwChOPBfx-ct2BdO2PQr5flHu5jjzzISTHWSrL7e0K6HJ0m9XM_D1xwf6bmRk9D6xzPZyrRDdIkJ8PqFndUG-DOlpCdrruGzyxjwkO7LvJmozOdg2Tpgnsakxrqx2h85YzVPD0w0kryMd5pm1LbUFVXoTrdXakLjAEV5hw66ChRwUGI9avw2FRjlLVLzXPna4GhW8_bZe3O-YvL8INb1i8yR2ILITyJLZBYULlqBQUO-dHdRzDgvHXo33e3cmd1umT4d4_Kzoq7-enluPv9jHjhvUUKX_ExRFZDeG55Y9K6yqrZqkulUH6iyuB5DFYkH-RMAfaiMgbHzxsx063kC7i7g9rCX8x48H2vt8ll6dOhoJr95lQzuJEl3pgx7EtxJ4r8is5W-jJOKcKVBPgNxmtwLUGHbVkVcq-TzpvIoWN0aH_V_rn9qUKcAxozc-1-0XPTaKMAgNGdOYUXLPlwK_q96qN7D2vD9wCsA6R4mp7pX5Z1myrUhG1dJ6P0a7FZ-FxJKySGvz0P_K386-otYyTNo7C15x4nfx2-3bcZAFVlvDdDfY9i1FvVNe44T1duHXll_JNRetXw1_HJG9zPmZ6L1jvd7PqFtEj3W11T-tvmS2EY8FEGOxzNcY";

        $cookie = new Cookie('Authorization', $token);
        $response = new Response();
        $response->headers->setCookie($cookie);
        $cookies = $response->headers->getCookies();
        return $response;
    }
}