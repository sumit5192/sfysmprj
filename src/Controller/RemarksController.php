<?php

namespace App\Controller;

use App\Entity\Student;
use App\Entity\Studentremarks;
use App\Form\StudentremarksFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/remarks", name="remarks.")

 */
class RemarksController extends AbstractController
{
    /**
     * @Route("/", name="list")
     * @param
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list()
    {

        //$studentremarks = $this->getDoctrine()->getRepository(Studentremarks::class)->findBy(array(['deletedAt'=>'']),array(['id'=>'asc']),1 ,0)[0];;
        $studentremarks = $this->getDoctrine()->getRepository(Studentremarks::class)->findBy(array('deletedAt' => NULL));

        return $this->render('admin/remarks/list.html.twig', [
            'studentremarksList' => $studentremarks
        ]);
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function new(Request $request)
    {
        $studentremarks = new Studentremarks();

        $form = $this->createForm(StudentremarksFormType::class,$studentremarks);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
           // dd($studentremarks);
            $em= $this->getDoctrine()->getManager();
            $em->persist($studentremarks);
            $em->flush();

            $this->addFlash('success','Record succesfully Added.');
            return $this->redirectToRoute('remarks.list');
        }

        return $this->render('admin/remarks/add.html.twig', [
            'formdata' => $form->createView()
        ]);
    }

    /**
     * @Route(path="/edit/{id}",name="edit")
     */
    public function edit(Studentremarks $studentremarks,Request $request)
    {
        $form =$this->createForm(StudentremarksFormType::class,$studentremarks);
        $form->handleRequest($request);
        if($form->isSubmitted())
        {
            //dd($student);

            $em= $this->getDoctrine()->getManager();
            $em->persist($studentremarks);
            $em->flush();

            $this->addFlash('success','Record succesfully Updated.');
            return $this->redirectToRoute('remarks.list');
        }


        return $this->render('admin/remarks/edit.html.twig', [
            'formdata' =>$form->createView(),
            'printlabel' =>'Edit Student Details'
        ]);

    }

    /**
     * @Route(path="/delete/{id}",name="delete")
     * @param Studentremarks $studentremarks
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Studentremarks $studentremarks)
    {
        $em= $this->getDoctrine()->getManager();
        $em->remove($studentremarks);
        $em->flush();

        $this->addFlash('success','Record succesfully Deleted.');
        return $this->redirectToRoute('remarks.list');
    }



}
