<?php

namespace App\Controller;

use App\Entity\ClassSection;
use App\Entity\Hobbie;
use App\Entity\Student;
use App\Entity\Studentremarks;
use App\Entity\TestMarkSheet;
use App\Form\StudentFormType;
use App\Repository\StudentremarksRepository;
use App\Repository\StudentRepository;
use App\Service\StudentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/student", name="student.")
 */
class StudentController extends Controller
{
    private $studentService;
    public function __construct(StudentService $studentService)
    {
        $this->studentService = $studentService;
    }

    /**
     * @Route("/", name="index",methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {

        $em= $this->getDoctrine();
        $knp_list = $em->getRepository(Student::class)->findBy(array('deletedAt' => NULL));
        $default_list = $em->getRepository(Student::class)->findBy(array('deletedAt' => NULL));
        $GetSearchVal = ''; // Default Null

        // $_GET parameters
        //$request->query->get('name');

        // $_POST parameters
        //$request->request->get('name');

        if($request->request->get('student-search'))
        {
            $GetSearchVal =trim($request->get('studentfilter'));
            $knp_list=$em->getRepository(Student::class)->findfilterStudentlist($GetSearchVal);


        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $knp_list, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            5/*limit per page*/
        );

        return $this->render('student/index.html.twig', [
            'studentlist' => $default_list,
            'pagination' => $pagination,
            'SearchVal' => $GetSearchVal
        ]);
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function create(Request $request)
    {
        $student = new Student();
        $form = $this->createForm(StudentFormType::class,$student);
        $form->handleRequest($request);
       // dump($form->getErrors());
        if($form->isSubmitted() && $form->isValid())
        {
            $em= $this->getDoctrine()->getManager();
            $em->persist($student);
            $em->flush();
            $this->addFlash('success','Record succesfully Added.');

            return $this->redirectToRoute('student.index');
        }

        return $this->render('student/create.html.twig', [
                'formdata' =>$form->createView(),
                'printlabel' =>'Add Student Details'
        ]);

    }


    /**
     * @Route(path="/edit/{id}",name="edit")
     * @param Request $request
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request,Student $student)
    {
        $formdata = [];
        $formdata['entity_manager'] = $this->getDoctrine()->getManager();
        $form =$this->createForm(StudentFormType::class,$student,$formdata);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em= $this->getDoctrine()->getManager();
            $em->persist($student);
            $em->flush();
            $this->addFlash('success','Record succesfully Updated.');
            return $this->redirectToRoute('student.index');
        }


        return $this->render('student/create.html.twig', [
            'formdata' =>$form->createView(),
            'printlabel' =>'Edit Student Details'
        ]);



    }

    /**
     * @Route("/show/{id}",name="show")
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\Response
     */
    /* Method 1
    public function show($id,StudentRepository $studentRepository)
    {
        $data = $studentRepository->find($id);
        dd($data);
    }
    */
    public function show(Student $student)
    {
        $GetmarksheetData = $this->studentService->getTestMarksheet($student);

        return  $this->render('student/show.html.twig',
            [
                'student' => $student,
                'TestData' => $GetmarksheetData
            ]);
    }


    /**
     * @Route(path="/displaytreewise/",name="displaytreewise")
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function studenttree()
    {
        $AplhaArray = $this->studentService->DisplaystudentTree();

        return  $this->render('student/studentdisplaytreewise.html.twig',[
                'student' => $AplhaArray
            ]);

    }

    /**
     * @Route(path="/delete/{id}",name="delete")
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Student $student)
    {
        $em= $this->getDoctrine()->getManager();
        $em->remove($student);
        $em->flush();

        $this->addFlash('success','Record succesfully Deleted.');
        return $this->redirectToRoute('student.index');
    }

    /**
     * @Route(path="/ajax/getClassSubDivisionList/",name="getClassSubDivisionList",methods={"POST"})
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function ajaxGetClassSubDivisionList(Request $request)
    {
        $classSectionId = $request->request->get('classSectionId');
        if($classSectionId === null)
        {
            return new JsonResponse('No Data');
        }
        else{
            $classSubDivisionArray = $this->studentService->GetClassSubDivisionList($classSectionId);
            return new JsonResponse($classSubDivisionArray);
        }

    }

}
