<?php
namespace App\DataFixtures;


use App\Entity\SubjectList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class SubjectFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $SubjectData= [ 'Hindi' , 'Gujarati', 'Sanskrit','Maths' ,'social science'];
        for($i=0;$i<=count($SubjectData)-1;$i++)
        {
            $Subject = new SubjectList();
            $Subject->setName($SubjectData[$i]);

            $manager->persist($Subject);
        }
        $manager->flush();
    }
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}