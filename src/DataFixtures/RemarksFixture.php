<?php

namespace App\DataFixtures;

use App\Entity\Student;
use App\Entity\Studentremarks;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Faker\Factory;


class RemarksFixture extends Fixture implements OrderedFixtureInterface
{

    protected $facker;
    public function load(ObjectManager $manager)
    {
        $this->facker =Factory::create();
        $studentListData = $manager->getRepository(Student::class)->findAll();
        for($i=1;$i<=9;$i++)
        {

            $studentremarks = new Studentremarks();
            $studentremarks->setRemarks($this->facker->text(20));
            $studentremarks->setStudent($studentListData[rand(0,8)]);
            $manager->persist($studentremarks);
        }
           $manager->flush();
    }

    public function getOrder()
    {
        return 8; // the order in which fixtures will be loaded
    }
}
