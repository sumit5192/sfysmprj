<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class UserCreateFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
         $user = new User();
         $user->setUsername('sumit');
         $user->setEmail('sumit@galitein.co.in');
         $user->setFirstname('sumit patel');
         $user ->setPlainPassword('sumit');
         $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('sumit_admin');
        $user->setEmail('sumit@galitein1.co.in');
        $user->setFirstname('sumit patel');
        $user ->setPlainPassword('sumit');
        $user->setEnabled(true);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEnabled(1);
        $manager->persist($user);

        $manager->flush();
    }

    public function getOrder()
    {
        return 6; // the order in which fixtures will be loaded
    }
}
