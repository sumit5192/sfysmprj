<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
class FacultyFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {


        $user = new User();
        $user->setUsername('sumit_faculty');
        $user->setEmail('faculty@galitein1.co.in');
        $user->setFirstname('sumit patel');
        $user ->setPlainPassword('sumit');
        $user->setEnabled(true);
        $user->setRoles(['ROLE_FACULTY']);
        $user->setEnabled(1);
        $user->setMobile('9825098021');
        $user->setAddress('Baroda');
        $user->setQualification('B.E I.T');
        $manager->persist($user);

        $manager->flush();
    }

    public function getOrder()
    {
        return 9; // the order in which fixtures will be loaded
    }
}