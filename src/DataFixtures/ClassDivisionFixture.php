<?php

namespace App\DataFixtures;


use App\Entity\ClassSubDivisionList;
use App\Entity\SubjectList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class ClassDivisionFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $ClassDivisionData= [ 'A' , 'B', 'C','D','E'];
        for($i=0;$i<=count($ClassDivisionData)-1;$i++)
        {
            $ClassSubDivision= new ClassSubDivisionList();
            $ClassSubDivision->setName($ClassDivisionData[$i]);

            $manager->persist($ClassSubDivision);
        }


        $manager->flush();
    }
    public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }
}