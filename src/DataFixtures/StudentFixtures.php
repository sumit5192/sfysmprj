<?php

namespace App\DataFixtures;

use App\Entity\Cast;
use App\Entity\ClassSection;
use App\Entity\ClassSectionSubDivision;
use App\Entity\ClassSubDivisionList;
use App\Entity\Student;
use App\Entity\StudentStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Faker\Factory;

class StudentFixtures extends Fixture implements OrderedFixtureInterface
{
    protected $facker;

    public function load(ObjectManager $manager)
    {
        $this->facker =Factory::create();
        $color = ['black','white','green'];
        $getClassSectionID = [];
        $classSectionData = $manager->getRepository(ClassSection::class)->findAll();
        $classSubDivisionListData = $manager->getRepository(ClassSubDivisionList::class)->findAll();
        $castData = $manager->getRepository(Cast::class)->findAll();
        $StudentStatusData = $manager->getRepository(StudentStatus::class)->findAll();


        foreach ($classSectionData as $data)
        {
            foreach ($data->getClassSectionSubDivisions() as $divisionData)
            {
                array_push($getClassSectionID,$divisionData->getId());
            }
        }

        for($i=1;$i<=9;$i++)
        {

         $student = new Student();
         $student->setName($this->facker->name);
         $student->setEmail($this->facker->email);
         $student->setMobile($this->facker->randomDigit());
         $student->setBithdate(new \DateTime());
         $student->setFavouriteColor(serialize([$color[rand(0,2)]]));
         $student->setClassSection($classSectionData[rand(0,8)]);
         $student->setClassSubDivision($classSubDivisionListData[rand(0,4)]);
         $student->setStatus($StudentStatusData[rand(0,2)]);
         $student->setCast($castData[rand(0,2)]);
         $manager->persist($student);
        }


        $manager->flush();
    }

    public function getOrder()
    {
        return 7; // the order in which fixtures will be loaded
    }
}
