<?php
namespace App\DataFixtures;


use App\Entity\StudentStatus;
use App\Entity\SubjectList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class StudentStatusFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $StudentStatusData= [ 'Fresh Join' , 'Move to Next Std', 'Leave it'];
        for($i=0;$i<=count($StudentStatusData)-1;$i++)
        {
            $StudentStatus = new StudentStatus();
            $StudentStatus->setName($StudentStatusData[$i]);

            $manager->persist($StudentStatus);
        }
        $manager->flush();
    }
    public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }
}