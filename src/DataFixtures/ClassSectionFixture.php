<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 13/3/20
 * Time: 6:10 PM
 */

namespace App\DataFixtures;
use App\Entity\ClassSection;
use App\Entity\ClassSectionSubDivision;
use App\Entity\ClassSubDivisionList;
use App\Entity\ClassSubject;
use App\Entity\SubjectList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Faker\Factory;

class ClassSectionFixture extends Fixture implements OrderedFixtureInterface
{
    protected $facker;


    protected $className = [ 'I (First)', 'II (Second)', 'III (Third)',' IV (Four)',' V (Five)',' VI (Six)',' VII (Seven)',' VIII (Eight)',' IX (Nine)', 'X (Ten)'];
    public function load(ObjectManager $manager)
    {
        $this->facker =Factory::create();
        $subjectArray = $SubDivisionArray= [];
        $subjectListData = $manager->getRepository(SubjectList::class)->findAll();
        $SubDivisionListDaData = $manager->getRepository(ClassSubDivisionList::class)->findAll();

        // First insert into division in classSectionDivision



        foreach ($subjectListData as $data)
        {
           array_push($subjectArray,$data);

        }
        foreach ($SubDivisionListDaData as $data)
        {
            array_push($SubDivisionArray,$data);

        }

        $count = rand(1,2);
        for($i=1;$i<=9;$i++)
        {

            $classSection = new ClassSection();
            $classSection->setName($this->className[$i]);


            for($j=0;$j<=$count;$j++)
            {
                $classSectionInstance = new ClassSectionSubDivision();
                $classSectionInstance->setClassSection($classSection);
                $classSectionInstance->setDivision($SubDivisionArray[$j]);
                $manager->persist($classSectionInstance);

                $classSubjectInsance = new ClassSubject();
                $classSubjectInsance->setClassSection($classSection);
                $classSubjectInsance->setSubject($subjectArray[$j]);
                $manager->persist($classSubjectInsance);

                $classSection->addClassSectionSubDivision($classSectionInstance);
                $classSection->addClassSubject($classSubjectInsance);
            }

            $manager->persist($classSection);
        }

        $manager->flush();

    }

    public function getOrder()
    {
        return 5; // the order in which fixtures will be loaded
    }
}