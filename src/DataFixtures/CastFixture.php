<?php

namespace App\DataFixtures;

use App\Entity\Cast;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class CastFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
       $castData= [ 'Open' , 'SC/ST', 'OBC' ];
       for($i=0;$i<=count($castData)-1;$i++)
        {
            $cast = new Cast();
            $cast->setName($castData[$i]);

            $manager->persist($cast);
        }


        $manager->flush();
    }
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}
