<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 8/5/20
 * Time: 11:55 AM
 */

namespace App\Service;


use App\Entity\ClassSection;
use App\Entity\Student;
use App\Entity\TestMarkSheet;
use Doctrine\ORM\EntityManagerInterface;

class StudentService
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getTestMarksheet(Student $student) :array
    {
        $GetmarksheetData = [];
        foreach($student->getTestMarkSheets() as $value)
        {

            $TestMarkSheet = $this->em->getRepository(TestMarkSheet::class)->find($value->getId());
            $marksheetArray = [];
            if(!empty($TestMarkSheet->getMarkssheets()))
            {
                $i =1;
                foreach ($TestMarkSheet->getMarkssheets() as $marks)
                {

                    $marksheetArray[$i]['marks'] = $marks->getMarks();
                    $marksheetArray[$i]['SubjectID'] = $marks->getSubject()->getValues()[0]->getId();
                    $marksheetArray[$i]['SubjectName'] = $marks->getSubject()->getValues()[0]->getName();
                    $marksheetArray[$i]['id'] = $marks->getId();
                    $i++;

                }
            }
            $GetmarksheetData[$value->getId()]['instance'] = $TestMarkSheet;
            $GetmarksheetData[$value->getId()]['MarksData'] = $marksheetArray;


        }
        return $GetmarksheetData;
    }

    public function GetClassSubDivisionList($classSectionId)
    {
        $classSection=  $this->em->getRepository(ClassSection::class)->find($classSectionId);
        $classSubDivisionArray = [];
        foreach ($classSection->getClassSectionSubDivisions() as $data)
        {
            $classSubDivisionArray[$data->getDivision()->getId()] = $data->getDivision()->getName();
        }
        return $classSubDivisionArray;
    }

    public function DisplaystudentTree()
    {
        $student = $this->em->getRepository(Student::class)->findAll();
        $AplhaArray=[];

        foreach ($student as $getstudent)
        {
            $arraykey =substr(ucwords($getstudent->getName('name')),0,1);
            if(!array_key_exists($arraykey ,$AplhaArray))
            {
                $AplhaArray[$arraykey]= [];
            }
            array_push($AplhaArray[$arraykey],$getstudent->getName('name'));

        }
        ksort($AplhaArray);
        return $AplhaArray;
    }
}