<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190906095701 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE studentremarks ADD student_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE studentremarks ADD CONSTRAINT FK_33EFB2D8CB944F1A FOREIGN KEY (student_id) REFERENCES student (id)');
        $this->addSql('CREATE INDEX IDX_33EFB2D8CB944F1A ON studentremarks (student_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE studentremarks DROP FOREIGN KEY FK_33EFB2D8CB944F1A');
        $this->addSql('DROP INDEX IDX_33EFB2D8CB944F1A ON studentremarks');
        $this->addSql('ALTER TABLE studentremarks DROP student_id');
    }
}
