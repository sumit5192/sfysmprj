<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190906070242 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE student ADD cast_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF3327B5E40F FOREIGN KEY (cast_id) REFERENCES cast (id)');
        $this->addSql('CREATE INDEX IDX_B723AF3327B5E40F ON student (cast_id)');
        $this->addSql('ALTER TABLE cast DROP FOREIGN KEY FK_12B8B9F6CB944F1A');
        $this->addSql('DROP INDEX UNIQ_12B8B9F6CB944F1A ON cast');
        $this->addSql('ALTER TABLE cast DROP student_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cast ADD student_id INT NOT NULL');
        $this->addSql('ALTER TABLE cast ADD CONSTRAINT FK_12B8B9F6CB944F1A FOREIGN KEY (student_id) REFERENCES student (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_12B8B9F6CB944F1A ON cast (student_id)');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF3327B5E40F');
        $this->addSql('DROP INDEX IDX_B723AF3327B5E40F ON student');
        $this->addSql('ALTER TABLE student DROP cast_id');
    }
}
