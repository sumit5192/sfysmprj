<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200313084555 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE class_section_sub_division (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE class_section_sub_division_class_section (class_section_sub_division_id INT NOT NULL, class_section_id INT NOT NULL, INDEX IDX_EEC56E20A27C0046 (class_section_sub_division_id), INDEX IDX_EEC56E206E2E11D8 (class_section_id), PRIMARY KEY(class_section_sub_division_id, class_section_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE class_section (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE class_section_sub_division_class_section ADD CONSTRAINT FK_EEC56E20A27C0046 FOREIGN KEY (class_section_sub_division_id) REFERENCES class_section_sub_division (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE class_section_sub_division_class_section ADD CONSTRAINT FK_EEC56E206E2E11D8 FOREIGN KEY (class_section_id) REFERENCES class_section (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE class_section_sub_division_class_section DROP FOREIGN KEY FK_EEC56E20A27C0046');
        $this->addSql('ALTER TABLE class_section_sub_division_class_section DROP FOREIGN KEY FK_EEC56E206E2E11D8');
        $this->addSql('DROP TABLE class_section_sub_division');
        $this->addSql('DROP TABLE class_section_sub_division_class_section');
        $this->addSql('DROP TABLE class_section');
    }
}
