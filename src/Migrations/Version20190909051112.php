<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190909051112 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE aadharcard DROP FOREIGN KEY FK_DA516837CA03521');
        $this->addSql('DROP INDEX UNIQ_DA516837CA03521 ON aadharcard');
        $this->addSql('ALTER TABLE aadharcard ADD aadharcardnumber VARCHAR(20) NOT NULL, DROP aadharcardnumber_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE aadharcard ADD aadharcardnumber_id INT DEFAULT NULL, DROP aadharcardnumber');
        $this->addSql('ALTER TABLE aadharcard ADD CONSTRAINT FK_DA516837CA03521 FOREIGN KEY (aadharcardnumber_id) REFERENCES student (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DA516837CA03521 ON aadharcard (aadharcardnumber_id)');
    }
}
