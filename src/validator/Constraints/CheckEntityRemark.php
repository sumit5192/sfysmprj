<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 19/9/19
 * Time: 2:22 PM
 */

namespace App\validator\Constraints;


use Symfony\Component\Validator\Constraint;
/**
 * @Annotation
 */
class CheckEntityRemark extends  Constraint
{
    public $message = 'The string "{{ string }}" contains an illegal character Not valid Special Character';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}