<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 19/9/19
 * Time: 11:40 AM
 */

namespace App\validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CheckAlphaValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {

        if (!ctype_alpha($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}