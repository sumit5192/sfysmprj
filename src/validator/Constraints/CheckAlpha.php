<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 19/9/19
 * Time: 11:39 AM
 */

namespace App\validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CheckAlpha extends Constraint
{
    public $message = 'The string "{{ string }}" contains an illegal character: it can only contain letters or numbers.';

    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }
}