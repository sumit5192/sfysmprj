<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 19/9/19
 * Time: 12:46 PM
 */

namespace App\validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CheckEntityHobbieValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {

        if(preg_match("/([%\$#\*]+)/", $value))
        {
            $this->context->buildViolation($constraint->message)
                ->atPath('descriptions')
                ->addViolation();
        }
    }

}