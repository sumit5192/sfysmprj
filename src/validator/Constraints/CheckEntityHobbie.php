<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 19/9/19
 * Time: 12:44 PM
 */

namespace App\validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CheckEntityHobbie extends Constraint
{
    public $message = 'The string "{{ string }}" contains an illegal character Not valid Special Character';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}