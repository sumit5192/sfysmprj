<?php
/**
 * Created by PhpStorm.
 * User: admin22
 * Date: 19/9/19
 * Time: 2:23 PM
 */

namespace App\validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CheckEntityRemarkValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {

        if(preg_match("/([%\$#\*]+)/", $value->getRemarks()))
        {

            $this->context->buildViolation($constraint->message)
                ->atPath('remarks')
                ->addViolation();
        }

    }
}